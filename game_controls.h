#ifndef GAME_CONTROLS_H
#define GAME_CONTROLS_H

#include "device.h"
typedef enum {	GameResult_MIN = -4,
				GameResult_HUMANWIN = -3, 
				GameResult_DRAW=0, 
				GameResult_MACHINEWIN = 3,
				GameResult_MAX = 4 } GameResult;

typedef struct {GameResult Score; 
				short X; 
} Best;

void initializeBoard();
void fillBoard(SquareState lt);
void fillBoard( SquareState state);
void showNewGame();
void waitForNewGame(short line[3]);
void think();
Input waitForMove(short ix);
Input enterOrDelay();

#endif
