#include "device.h"
#include "game_controls.h"

BoardSquare board[3][3];
extern short isNewGame;

void fillBoard( SquareState state)
{
  short i;
  for (i = 0; i != 9; ++i) {
	board[0][i].square = state;
  }
  showBoard();
}

void initializeBoard()
{
  initializeEnter();

  board[0][0].showSquare = squareA1;
  board[0][1].showSquare = squareA2;
  board[0][2].showSquare = squareA3;
;
  board[1][0].showSquare = squareB1;
  board[1][1].showSquare = squareB2;
  board[1][2].showSquare = squareB3;
;
  board[2][0].showSquare = squareC1;
  board[2][1].showSquare = squareC2;
  board[2][2].showSquare = squareC3;

  fillBoard(SquareState_EMPTY);
}

void showNewGame()
{
    fillBoard(SquareState_HUMAN);
    ldelay(1000);
    fillBoard(SquareState_MACHINE);
    ldelay(1000);
    fillBoard(SquareState_EMPTY);
}

Input enterOrDelay()
{
  short n;
  Input ret;
  for(n=0; n!=5; ++n)
  {
    ret = checkInput();
    if(ret == Input_ENTER){
      return ret;
    }
  }
  return Input_UNDEFINED;
}

void waitForNewGame(short line[3])
{
  SquareState lightBuffer[9];
  short i;
  ldelay(500);
  for(i=0; i!=9;++i) { // store board
	  lightBuffer[i] = board[0][i].square;
  }
  for(;;)
  {
	  // turn light off ...
      if (line[0] == -1) { 
		fillBoard(SquareState_EMPTY);
	  } else {
		  for (i=0; i != 3; ++i) {
			  board[0][line[i]].square = SquareState_EMPTY;
		  }
	  }
      showBoard();
	  ldelay(200);
      if(enterOrDelay()==Input_ENTER) {
        return;
      }
	  for (i=0; i!=9; ++i) { // restore board
		board[0][i].square = lightBuffer[i];
      }
      showBoard();
	  ldelay(200);
      if(enterOrDelay()==Input_ENTER) {
        return;
      }
    }
}

void think()
{
  short i,n;
  short numOfEmptySquares;
  int tSleep;
  for (i=0, numOfEmptySquares = 0; i!=9; ++i) {
	  if (board[0][i].square == SquareState_EMPTY) {
		  ++numOfEmptySquares;
	  }
  }
  if(numOfEmptySquares <=1) {
    return;
  }
  tSleep = 400/numOfEmptySquares;
  for(n=0;n!=3;++n) 
  { // repeat 3 times 
    for (i =0; i != 9; ++i) {
      if(board[0][i].square == SquareState_EMPTY){ 
        board[0][i].square = SquareState_MACHINE;
        showBoard();
        ldelay(tSleep);
        board[0][i].square = SquareState_EMPTY;
        showBoard();
      }
    }
  }
} 

Input waitForMove(short ix)
{ // Never return Input_UNDEFINED
	Input ret;
	short n;
	for(;;)
	{
		board[0][ix].square = SquareState_HUMAN;
		showBoard();
		for(n=0;n!=3;++n)
		{
		  ret = checkInput();
		  if (ret != Input_UNDEFINED)
		  {
			if(ret != Input_ENTER){
			  board[0][ix].square = SquareState_EMPTY;
			}
			return ret;
		  }
		}
		board[0][ix].square = SquareState_EMPTY;
		showBoard();
		for(n=0;n!=3;++n)
		{
		  ret = checkInput();
		  if (ret != Input_UNDEFINED)
		  {
				if (ret == Input_ENTER) {
				  board[0][ix].square = SquareState_HUMAN;
				  showBoard();
				}
				return ret;
		  }
		}
	}
}


