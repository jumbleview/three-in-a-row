#include <avr/io.h>

#include "game_controls.h"
#include "device.h"
#include <avr/interrupt.h>

#define DMAX1 1 
#define DMAX2 2
#define NOISE_LIMIT 1

extern BoardSquare board[3][3];

typedef enum {OUT_LOW, OUT_HIGH} OutputLevel;

// macros for digital input/output

#define pinOut(port, bit, outLevel) \
	(DDR##port |= (1 << ( DD##port##bit)));\
	switch(outLevel) \
	{\
	case OUT_LOW: (PORT##port &= ~(1 << (PORT##port##bit))); break;\
	case OUT_HIGH: (PORT##port |= (1 << (PORT##port##bit))); break;\
	}


#define pinIn(port,bit) \
	((DDR##port &= ~(1 << (DD##port##bit))),\
	(PORT##port |= (1 << (PORT##port##bit))),\
	(PIN##port & (1 << (PORT##port##bit))))

/* Example of pinIn macro expansion for port D and bit 2
  (DDRD &= ~(1 << DDD2)), 		// signal direction IN
  (PORTD |= (1 << PORTD2)),	// pull up resistor activated
  (PIND & (1 << PORTD2)))	// reading input: 0 if connected to the ground
*/


volatile unsigned short forward=0;
volatile unsigned short back=0;
volatile unsigned short time=0;

volatile unsigned short oldForward=0;
volatile unsigned short oldBack=0;

volatile unsigned short dmax = DMAX1;
volatile unsigned short ix = 0;
volatile unsigned short oldA = 0;

volatile short aLevelOld;
volatile short bLevelOld;
volatile short aLevel;
volatile short bLevel;
volatile short pushSignal;
volatile short pulses;

volatile short isShatterLocked;

void inActivate()
{
	aLevelOld = pinIn(D,6);
	bLevelOld = pinIn(D,5);
	pushSignal = 0;
	isShatterLocked = 0;
}


void timer0_activate(unsigned short code)
{
  cli();
  time = 0;
  TCCR0A=0; // Normal opearation
  TCCR0B=2; // f devider 8  : about 2 mls for interrupt
  TIMSK0 = 1;
  sei();
}

// Vector to process timer interrupt

ISR(TIMER0_OVF_vect)
{
	aLevel = pinIn(D,6);
	bLevel = pinIn(D,5);		
	if (!isShatterLocked)
	{
		if ((aLevelOld != 0) && (aLevel == 0))
		{
			isShatterLocked = 1;
			if (bLevel == 0)
			{
				++pulses;
				++forward;
			} else {
				--pulses;
				++back;
			}
		}
	}
	if (bLevel != bLevelOld)
	{
		isShatterLocked = 0;
	}
	aLevelOld = aLevel;
	bLevelOld = bLevel;
	pushSignal = pinIn(B,7);
	++time;
}



void ldelay(long tmm)
{ 
  unsigned short delay = tmm/2;
  unsigned short start = time;

  while(1)
  {
  	volatile short ix;
  	for(ix=0; ix != 100; ++ix);
	unsigned short delta = time - start;
	if(delta > delay)
		break;
  }
}

short getRandomFactor(short divider) // number 0..divider-1 choosen randomly based on time
{
	return time%divider;
}

void initializeEnter()
{
  forward = 0;
  back = 0;
  time = 0;
  oldForward = forward;
  oldBack = back;
  dmax = DMAX1;
  ix = 0;

  inActivate();	
  timer0_activate(0);
}

Input checkInput()
{
  ldelay(50);
  Input ret = Input_UNDEFINED;
  if(pushSignal == 0)
  { // Enter pressed
	ret = Input_ENTER;
  } else {
	  if((forward - oldForward) >= NOISE_LIMIT)
	  {
		ret = Input_FORWARD;
	  } else if((back - oldBack) >= NOISE_LIMIT) 
	  {
		ret = Input_BACKWARD;
	  }
  }
  if(ret != Input_UNDEFINED)
  {
    oldForward = forward;
  	oldBack = back;
  }
  return ret;
}


void squareA1(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(C,3,OUT_LOW); // a1
		pinOut(C,4,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(C,3,OUT_LOW); // a1
		pinOut(C,4,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(C,3,OUT_HIGH); // a1
		pinOut(C,4,OUT_LOW);
    break;
  }
}

void squareA2(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(B,5,OUT_LOW); // a2
		pinOut(C,0,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(B,5,OUT_LOW); // a2
		pinOut(C,0,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(B,5,OUT_HIGH); // a2
		pinOut(C,0,OUT_LOW);
    break;
  }
}

void squareA3(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(B,3,OUT_LOW);  // a3
		pinOut(B,4,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(B,3,OUT_LOW);  // a3
		pinOut(B,4,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(B,3,OUT_HIGH);  // a3
		pinOut(B,4,OUT_LOW);
    break;
  }
}

void squareB1(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(D,0,OUT_LOW); // b1
		pinOut(C,5,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(D,0,OUT_LOW); // b1
		pinOut(C,5,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(D,0,OUT_HIGH); // b1
		pinOut(C,5,OUT_LOW);
    break;
  }
}

void squareB2(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(C,2,OUT_LOW); // b2
		pinOut(C,1,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(C,2,OUT_LOW); // b2
		pinOut(C,1,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(C,2,OUT_HIGH); // b2
		pinOut(C,1,OUT_LOW);
    break;
  }
}
void squareB3(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(B,1,OUT_LOW);  // b3
		pinOut(B,2,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(B,1,OUT_LOW);  // b3
		pinOut(B,2,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(B,1,OUT_HIGH);  // b3
		pinOut(B,2,OUT_LOW);
    break;
  }
}

void squareC1(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(D,1,OUT_LOW); // c1
		pinOut(D,4,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(D,1,OUT_LOW); // c1
		pinOut(D,4,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(D,1,OUT_HIGH); // c1
		pinOut(D,4,OUT_LOW);
    break;
  }
}

void squareC2(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(D,2,OUT_LOW); // c2
		pinOut(D,3,OUT_LOW);
    break;
  case  SquareState_MACHINE:
		pinOut(D,2,OUT_LOW); // c2
		pinOut(D,3,OUT_HIGH);
    break;
  case  SquareState_HUMAN:
		pinOut(D,2,OUT_HIGH); // c2
		pinOut(D,3,OUT_LOW);
    break;
  }
}
 
void squareC3(SquareState lt)
{
  switch(lt)
  {
  case  SquareState_EMPTY:
		pinOut(B,0,OUT_LOW);  // c3
		pinOut(D,7,OUT_LOW); 
    break;
  case  SquareState_MACHINE:
		pinOut(B,0,OUT_LOW);  // c3
		pinOut(D,7,OUT_HIGH);
			break;
  case  SquareState_HUMAN:
		pinOut(B,0,OUT_HIGH);  // c3
		pinOut(D,7,OUT_LOW); 
	   break;
  }
}

void showBoard()
{
  int ix;
	for(ix = 0; ix != 9; ++ix) {
      board[0][ix].showSquare(board[0][ix].square);
	}
}
