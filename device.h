#ifndef DEVICE_H
#define DEVICE_H

typedef enum {	SquareState_HUMAN = - 1, 
				SquareState_EMPTY = 0, 
				SquareState_MACHINE = 1} SquareState;

typedef enum {Input_UNDEFINED, 
			  Input_FORWARD, 
			  Input_BACKWARD, 
			  Input_ENTER} Input;

typedef void (*Showsquare)(SquareState state);

typedef struct {
  SquareState square;
  Showsquare showSquare;
} BoardSquare;


void squareA1(SquareState state);
void squareA2(SquareState state);
void squareA3(SquareState state);
void squareB1(SquareState state);
void squareB2(SquareState state);
void squareB3(SquareState state);
void squareC1(SquareState state);
void squareC2(SquareState state);
void squareC3(SquareState state);

void showBoard();
void ldelay(long time);

Input checkInput();
void initializeEnter();
short getRandomFactor(short divider); // number 0..devider-1 choosen randomly based on time
#endif
