#include "device.h"
#include "game_controls.h"

BoardSquare board[3][3];

short moveCounter;

void storeWinLine(short line[3], short ix0, short ix1, short ix2)
{
	line[0] = ix0;
	line[1] = ix1;
	line[2] = ix2;
}

GameResult evaluate(short line[3])
{
    short ix;
    short iy;
    short sum;

	GameResult rc = GameResult_DRAW;
    for ( iy = 0; iy != 3; ++iy)
    {// test horizontal
        for (sum = 0, ix = 0; ix != 3; ++ix)
        {
             if (board[iy][ix].square == SquareState_EMPTY)
             {
                rc = GameResult_MIN;
                break;
             }
                 sum += (short)board[iy][ix].square;
             }
                if ( sum == 3 || sum == -3)
                {
					storeWinLine(line, iy*3, iy*3 + 1, iy*3 +2);
                    return (GameResult)sum;
                }
            }

            for (ix = 0; ix != 3; ++ix)
            {// test vertical
                for (sum = 0, iy = 0; iy != 3; ++iy)
                {
					if (board[iy][ix].square == SquareState_EMPTY)
                    {
                        rc = GameResult_MIN;
                        break;
                    }
                    sum += (short)board[iy][ix].square;
                }
                if (sum == 3 || sum == -3)
                {
					storeWinLine(line, ix, ix + 3, ix + 6);
					return (GameResult)sum;
                }
            }
            for (sum = 0, ix = 0, iy = 0; ix != 3; ++ix, ++iy)
            { // left diagonal ...
                if (board[iy][ix].square == SquareState_EMPTY)
                {
                    rc = GameResult_MIN;
                    break;
                }
                sum += (short)board[iy][ix].square;
            }
            if (sum == 3 || sum == -3)
            {
				storeWinLine(line,0,4,8);
				return (GameResult)sum;
            }
            for (sum = 0, ix = 2, iy = 0; ix != -1; --ix, ++iy)
            { // right diagonal ...
                if (board[iy][ix].square == SquareState_EMPTY)
                {
                    rc = GameResult_MIN;
                    break;
                }
                sum += (short)board[iy][ix].square;
            }
            if (sum == 3 || sum == -3)
            {
				storeWinLine(line,2,4,6);
				return (GameResult)sum;
            }
            return rc;
}

GameResult humanMove(short line[3])
{
	int ix=0;
	Input ret;
	line[0] = -1;
	++moveCounter;
	for (; ix != 9; ++ix) 
	{
		if(board[0][ix].square == SquareState_EMPTY) {
			break;
		}
	}
	ldelay(300);
	for (;;)
	{
		ret = waitForMove(ix);
		if (ret == Input_ENTER) {
			break;
		}
		if (ret == Input_FORWARD)
		{
			do {
				++ix;
				if (ix == 9) ix = 0;
			} while (board[0][ix].square != SquareState_EMPTY);
		} else if (ret == Input_BACKWARD) {
			do {
				--ix;
				if (ix == -1) ix = 8;
			} while (board[0][ix].square != SquareState_EMPTY);
		}
	}
	showBoard();
	return evaluate(line);
}
 
Best chooseMove(short isMachine, short line[3])
{
    Best myBest;
    Best reply;
	short ix;
    SquareState side = SquareState_MACHINE;
	line[0] = -1;
	myBest.Score = evaluate(line);
    myBest.X = -1;

    if (myBest.Score != GameResult_MIN)
    {
            return myBest;
    }
    if (!isMachine)
    {
        side = SquareState_HUMAN;
        myBest.Score = GameResult_MAX;
    }
    for (ix = 0; ix != 9; ++ix)
    {
        if (board[0][ix].square == SquareState_EMPTY)
        { // make a move
            board[0][ix].square = side;
            reply = chooseMove(!isMachine,line);
            // undo move ...
            board[0][ix].square = SquareState_EMPTY;
            if ((isMachine && (reply.Score > myBest.Score)) ||
                (!isMachine && (reply.Score < myBest.Score)))
            { // store move
                myBest.Score = reply.Score;
                myBest.X = ix;
            } else if (isMachine && (reply.Score == myBest.Score)) { 
				// if the score is the same give preference for corners or center
				// corners and center have even square number
				if ((ix & 1) == 0 ) {
					 myBest.X = ix;
				}
			}
        }
    }
    return myBest;
}

Best chooseMoveSimplePruning(short isMachine, short line[3])
{
    Best myBest;
    Best reply;
	short ix;
    SquareState side = SquareState_MACHINE;
	line[0] = -1;
	myBest.Score = evaluate(line);
    myBest.X = -1;

    if (myBest.Score != GameResult_MIN)
    {
            return myBest;
    }
    if (!isMachine)
    {
        side = SquareState_HUMAN;
        myBest.Score = GameResult_MAX;
    }
    for (ix = 0; ix != 9; ++ix)
    {
        if (board[0][ix].square == SquareState_EMPTY)
        { // make a move
            board[0][ix].square = side;
            reply = chooseMoveSimplePruning(!isMachine,line);
            // undo move ...
            board[0][ix].square = SquareState_EMPTY;
            if ((isMachine && (reply.Score > myBest.Score)) ||
                (!isMachine && (reply.Score < myBest.Score)))
            { // store move
                myBest.Score = reply.Score;
                myBest.X = ix;
            } 
			if (isMachine) {
              if (reply.Score == GameResult_MACHINEWIN) {
                    return myBest;
               }
			} else {
              if (reply.Score == GameResult_HUMANWIN) {
                    return myBest;
               }
			}
        }
    }
    return myBest;
}


GameResult machineMove(short line[3])
{
	Best move;
	short i;
	if (moveCounter == 0) 
	{
		move.X = getRandomFactor(5)*2;
	} else if (moveCounter == 1 ) {
		if ( board[1][1].square == SquareState_EMPTY)
		{
			move.X = 4; // central square
		} else {
			move.X  = getRandomFactor(4)*2;
			if (move.X > 2) 
			{
				move.X += 2; // any corner square
			}
		}
	} else {
		// move = chooseMove(1, line); // simple pruning decreases number of iteration in 5 times ...
		move = chooseMoveSimplePruning(1, line);
	}
	think();
	++moveCounter;
	for (i = 0; i != 3; ldelay(200), ++i) {
		board[0][move.X].square = SquareState_EMPTY;
		showBoard();
		ldelay(200);
		board[0][move.X].square = SquareState_MACHINE;
		showBoard();
	}
	return evaluate(line);
}

void theGame()
{
  GameResult result;
  short line[3];
  initializeBoard();
  for (;;)
  {
		showNewGame();
		moveCounter = 0;
		for(;;)
		{
			result = humanMove(line);
			if (result  != GameResult_MIN) {
				break;
			}
			result = machineMove(line);
			if(result != GameResult_MIN){
				break;
			}
		}
		waitForNewGame(line);
		showNewGame();
		moveCounter = 0;
		for(;;)
		{
			result = machineMove(line);
			if(result != GameResult_MIN){
				break;
			}
			result = humanMove(line);
			if (result  != GameResult_MIN) {
				break;
			}
		}
		waitForNewGame(line);
	}
}
